//
//  AppDelegate.swift
//  SferaFBAuthApp
//
//  Created by Vladimir Moskalev on 24.02.2020.
//  Copyright © 2020 vmoskalev. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        guard let window = self.window else {return false}
        
        FirebaseApp.configure()
        Auth.auth().signInAnonymously() {authResult, error in
            guard let user = authResult?.user else {return}
            
            let view = ViewController()
            view.uid = user.uid
            window.rootViewController = view
            window.makeKeyAndVisible()
            
            
            
//            var ref: DatabaseReference = Database.database().reference()
//            ref.child("users").child(user.uid).setValue(["uid": user.uid, "name":UIDevice.current.name, "model":UIDevice.current.model, "version":UIDevice.current.systemVersion])
        }
        
        return true
    }
}
