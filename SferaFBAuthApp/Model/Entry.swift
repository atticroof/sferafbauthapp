//
//  Entry.swift
//  SferaFBAuthApp
//
//  Created by Vladimir Moskalev on 24.02.2020.
//  Copyright © 2020 vmoskalev. All rights reserved.
//

import Foundation
import ObjectMapper
import Firebase

class Entry: Mappable {
    var uid: String?
    var name: String?
    var model: String?
    var version: String?
    
    func mapping(map: Map) {
        uid <- map["uid"]
        name <- map["name"]
        model <- map["model"]
        version <- map["version"]
    }
    
    required init?(map: Map) {}
    
    convenience init?(ds: DataSnapshot) {
        guard let json = ds.value as? [String: Any] else {return nil}
        self.init(JSON: json)
    }
}
