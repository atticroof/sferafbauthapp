//
//  ViewController.swift
//  SferaFBAuthApp
//
//  Created by Vladimir Moskalev on 24.02.2020.
//  Copyright © 2020 vmoskalev. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import Firebase
import RxFirebase

class ViewController:UIViewController {

    private var didSetup = false
    
    private var btnAdd:UIButton = UIButton()
    private var btnRemove:UIButton = UIButton()
    private var table:UITableView = UITableView()
    
    private lazy var bag:DisposeBag = {return DisposeBag()}()
    private lazy var ref:DatabaseReference = {return Database.database().reference()}()
    
    private var entries:BehaviorRelay<[Entry]> = BehaviorRelay(value: [])

    var uid:String! = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "ТЗ для Sfera"
        self.view.backgroundColor = UIColor.white
        
        self.setupButtons()
        self.view.addSubview(self.btnAdd)
        self.view.addSubview(self.btnRemove)
        
        self.setupTable()
        self.view.addSubview(self.table)
        
        self.setupFirebaseHandler()
        
        self.view.setNeedsUpdateConstraints()
    }
    
    private func setupTable() {
        self.table.register(FastCell.self, forCellReuseIdentifier: "FastCell")
        self.table.rowHeight = 80
        self.entries.bind(to: self.table.rx.items) { (tableView, row, entry) in
            let cell = tableView.dequeueReusableCell(withIdentifier: "FastCell") as! FastCell
            cell.entry = entry
            return cell
        }.disposed(by: self.bag)
    }
    
    private func setupButtons() {
        self.btnAdd.setTitle("добавиться в базу", for: .normal)
        self.btnAdd.backgroundColor = .systemGreen
        self.btnAdd.rx.tap.bind {
            self.ref.child("users").child(self.uid).setValue(["uid": self.uid, "name":UIDevice.current.name, "model":UIDevice.current.model, "version":UIDevice.current.systemVersion])
        }.disposed(by: bag)
        
        self.btnRemove.setTitle("удалиться из базы", for: .normal)
        self.btnRemove.backgroundColor = .systemRed
        self.btnRemove.rx.tap.bind {
            self.ref.child("users").child(self.uid).removeValue()
        }.disposed(by: bag)
    }
    
    private func setupFirebaseHandler() {
        self.ref.child("users").observe(.value) { snapshot in
            self.entries.accept(snapshot.children.allObjects.map{Entry.init(ds: $0 as! DataSnapshot)!})
        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if !didSetup {
            didSetup = true
            
            self.btnAdd.snp.makeConstraints { make in
                make.height.equalTo(40)
                make.top.equalTo(view).offset(40)
                make.leading.equalTo(12)
                make.trailing.equalTo(-12)
            }
            
            self.btnRemove.snp.makeConstraints { make in
                make.height.equalTo(40)
                make.top.equalTo(self.btnAdd.snp.bottom).offset(6)
                make.leading.equalTo(12)
                make.trailing.equalTo(-12)
            }
            
            self.table.snp.makeConstraints { make in
                make.top.equalTo(self.btnRemove.snp.bottom).offset(12)
                make.leading.equalTo(self.view).offset(12)
                make.trailing.equalTo(self.view).offset(-12)
                make.bottom.equalTo(self.view).offset(-12)
            }
        }
    }
}

class FastCell:UITableViewCell {
     var entry:Entry?
    
    private var labelUID:UILabel = UILabel()
    private var labelName:UILabel = UILabel()
    private var labelOther:UILabel = UILabel()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.labelUID.text = "uid = \(self.entry?.uid ?? "")"
        self.labelName.text = "name = \(self.entry?.name ?? "")"
        self.labelOther.text = "model = \(self.entry?.model ?? "") iOS = \(self.entry?.version ?? "") "
        
        self.addSubview(labelUID)
        self.addSubview(labelName)
        self.addSubview(labelOther)
        
        self.labelUID.snp.makeConstraints { make in
            make.top.equalTo(self)
            make.left.equalTo(self)
        }
        
        self.labelName.snp.makeConstraints { make in
            make.top.equalTo(self.labelUID.snp.bottom).offset(4)
            make.left.equalTo(self)
        }
        
        self.labelOther.snp.makeConstraints { make in
            make.top.equalTo(self.labelName.snp.bottom).offset(4)
            make.left.equalTo(self)
        }
    }
}
